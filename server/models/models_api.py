import json
from flask import jsonify, make_response
from flask_restx import Namespace, Resource, fields

from models.survival import Model, MODEL_TYPES, MODELS_LIST

api = Namespace("Models", description="Expiration models")

model = Model()

params_model_type = api.model(
    "Model",
    {
        "name": fields.String(required=True, description="Model name"),
        "type": fields.String(required=True, description="Model type"),
    },
)

params_set_model = api.model('Set Model', {
    'model_type': fields.String(required=True, description='Model type', default='Kaplan-Meier')
})

params_fit_model = api.model('Fit Model', {
    'specification': fields.String(required=False, description='Specification for parametric models'),
    'fit_intercept': fields.Boolean(required=False, description='Add intercept'),

    'id_col': fields.String(required=False, description='ID column (Subject)'),
    'start_col': fields.String(required=False, description='Start column (Enter)'),
    'stop_col': fields.String(required=False, description='Stop column (Exit)'),
    'event_col': fields.String(required=False, description='Event column (Death)'),
})

params_restore_model = api.model('Restore Model', {
    'path': fields.String(required=False, description='Path to model file'),
    'model_type': fields.String(required=False, description='Model type to restore'),
    'model_uid': fields.String(required=False, description='Model uid to restore'),
})


@api.route("/types", methods=["GET"])
class AvailableModelTypes(Resource):
    def get(self):
        """List of all available model types"""
        return jsonify(MODEL_TYPES)


@api.route("/types/<name>", methods=["GET"])
@api.param("name", "Model type")
class ModelType(Resource):
    @api.doc(params={'name': 'Model'})
    def get(self, name):
        """Get type of model by its name"""
        for model in MODEL_TYPES:
            if model["name"] == name:
                return jsonify(model)
        return f"Model '{name}' not found in available models", 501


@api.route("/set", methods=["POST"])
class SetModel(Resource):
    @api.expect(params_set_model)
    def post(self):
        """Set model type"""
        model_type = api.payload.get('model_type')
        if model_type not in model.models_list:
            return f"Model '{model_type}' not found in available models", 501
        if model.model_type == model_type:
            return f"Model {model_type} already set", 200

        model.clear_model()
        model.set_model_type(model_type)
        return f"Set model to {model_type}", 201


@api.route("/fit", methods=["POST"])
class FitModel(Resource):
    @api.expect(params_fit_model)
    def post(self):
        """Fit selected model"""
        model.fit(**api.payload)
        return f"Fitted model {model.model_type}", 201


@api.route("/save", methods=["GET"])
class SaveCurrentModel(Resource):
    def get(self):
        """Save current model"""
        message = model.save()
        return message, 200


@api.route("/restore", methods=["POST"])
class LoadFittedModel(Resource):
    @api.expect(params_restore_model)
    def post(self):
        """Restore fitted model"""
        message = model.restore(**api.payload)
        return message, 200


@api.route("/fitted", methods=["GET"])
class ListAllFitted(Resource):
    def get(self):
        """List of all fitted models"""
        model.update_storage()
        return model.fitted, 200


@api.route("/fitted/<name>", methods=["GET"])
@api.param("name", "Model type")
class ListAllFittedType(Resource):
    @api.doc(params={'name': 'Model'})
    def get(self, name):
        """List all fitted models of chosen type"""
        if name in model.models_list:
            return jsonify(model.fitted[name])
        return f"Model '{name}' not found in available models", 501

