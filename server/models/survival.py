import pickle
import numpy as np
import pandas as pd
from datetime import datetime
from pathlib import Path

from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL

import statsmodels.api as sm
from lifelines import KaplanMeierFitter
from models.cox_fitter import CoxModelFitter


MODEL_TYPES = [
    {'name': 'Logit', 'type': 'parametric'},
    {'name': 'Kaplan-Meier', 'type': 'nonparametric'},
    {'name': 'Cox', 'type': 'semiparametric'},
]

MODELS_LIST = [model_type['name'] for model_type in MODEL_TYPES]

CHECKPOINT_DIR = './.tmp'

CONF_DB = {
    'database'      : 'postgres',
    'user'          : 'postgres',
    'host'          : 'postgres',
    'port'          : '5432',
    'password'      : 'password',
    'container_name': 'postgres',
}


class Model(object):

    def __init__(self, db_conf=None) -> None:
        self.models_list = MODELS_LIST
        self.clear_model()
        self.set_model_type('Kaplan-Meier')
        self.update_storage()
        self.dbc = self.connect_db(db_conf)
        self.data = self.get_data(table='survival')

    def clear_model(self):
        self.model = None

    def connect_db(self, db_conf=None):
        db_conf = CONF_DB if db_conf is None else db_conf
        url_db = URL.create(
            "postgresql+psycopg2",
            username=db_conf.get('user'),
            password=db_conf.get('password'),
            host=db_conf.get('host'),
            port=db_conf.get('port'),
            database=db_conf.get('database'),
        )
        return create_engine(url_db)

    def get_data(self, table=None):
        return pd.read_sql(table, con=self.dbc)

    def set_model_type(self, model_name: str):
        if model_name not in MODELS_LIST:
            raise KeyError(f'Model type with id {model_name} does not exist')
        if hasattr(self, 'model_type'):
            if self.model_type != model_name:
                self.clear_model()
        self.model_type = model_name
        return True

    def fit_logit(self, specification: list = None, fit_intercept: bool = False):
        if specification is None:
            if fit_intercept:
                specification = ['intercept']
                train_df = np.ones(self.data.shape[0])
        else:
            train_df = self.data[specification].copy()
            if fit_intercept:
                specification = specification + ['intercept']
                train_df['intercept'] = np.ones(train_df.shape[0])
        self.clear_model()
        fitted = sm.Logit(self.data['dmIsExpired'], train_df).fit(disp=False)
        return fitted

    def fit_km(self, stop_col: str = 'nMonth', event_col: str = 'dmIsExpired'):
        self.clear_model()
        fitted = KaplanMeierFitter().fit(self.data[stop_col], self.data[event_col])
        return fitted

    def fit_cox(self, id_col: str = 'nAgrmntId', start_col: str = None, stop_col: str = 'nMonth', 
                event_col: str = 'dmIsExpired', specification: list = None):
        train_df = self.data
        if start_col is None:
            start_col = 'start_col'
            train_df[start_col] = train_df[stop_col] - 1
        
        varlist = specification + [id_col, start_col, stop_col, event_col]
        train_df = train_df[varlist]
        self.clear_model()
        fitted = CoxModelFitter().fit(train_df, id_col=id_col, event_col=event_col,
                                      start_col=start_col, stop_col=stop_col)
        return fitted

    def fit(self, specification: list = None, fit_intercept: bool = False, **kwargs):
        
        if self.model_type == 'Logit':
            if not fit_intercept and specification is None:
                raise ValueError('''Empty specification without intercept 
                                 allowed only for Kaplan-Meier estimator''')
            self.model = self.fit_logit(specification, fit_intercept)
        
        elif self.model_type == 'Kaplan-Meier':
            if fit_intercept:
                return 'Intercept cannot be set. Kaplan-Meier is non-parametric estimator'
            if specification is not None:
                return 'Specification cannot be set. Kaplan-Meier is non-parametric estimator'
            self.model = self.fit_km()
        
        elif self.model_type == 'Cox':
            if fit_intercept:
                return 'Cox regression does not allow intercept'
            if specification is None:
                return 'Cox regression with empty specification fails to Kaplan-Meier estimator'
            self.model = self.fit_cox(specification=specification, **kwargs)
        
        else:
            return None
        
        self.model_uid = datetime.now().strftime('%y%m%d_%H%M%S')
        return True

    def summary(self):
        if self.model_type == 'Logit':
            return self.model.summary()
        elif self.model_type == 'Kaplan-Meier':
            return self.model.survival_function_
        elif self.model_type == 'Cox':
            return self.model.print_summary()
        else:
            return "Error, model type not found"

    def predict(self, data=None):
        if data is None:
            data = self.data
        try:
            result = self.model.predict(data)
            return result
        except Exception as e:
            return e

    def update_storage(self):
        path = Path(f'{CHECKPOINT_DIR}/storage/').glob('**/*')
        fitted = {i: [] for i in self.models_list}
        for x in path:
            if x.is_file():
                fitted[x.parent.name].append(x.name)
        self.fitted = fitted

    def save(self, path=None):
        if path is None:
            path = f'{CHECKPOINT_DIR}/storage/{self.model_type}/{self.model_uid}'
        Path(path).parent.mkdir(parents=True, exist_ok=True)
        with open(path, 'wb') as f:
            pickle.dump(self.model, f)
        self.update_storage()
        return f'Saved model {self.model_type} {self.model_uid} to {path}'

    def restore(self, path=None, model_type=None, model_uid=None):
        if path is not None:
            with open(path, 'rb') as f:
                self.model = pickle.load(f)
            return f'Load model from {path}'
        
        self.update_storage()
        if model_type is None:
            # If model type not provided use current
            model_type = self.model_type
        if model_uid is None:
            # If model uid not provided use last model
            model_uid = sorted(self.fitted[model_type])[-1]
        
        assert model_type in self.models_list, f'Model type {model_type} does not exist'
        assert model_uid in self.fitted[model_type], f'Model {model_uid} does not exist'
    
        path = f'{CHECKPOINT_DIR}/storage/{model_type}/{model_uid}'
        with open(path, 'rb') as f:
            self.model = pickle.load(f)
        self.model_type = model_type

        return f'Load model from {path}'
