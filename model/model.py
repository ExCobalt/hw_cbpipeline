import time
import logging
import datetime
from tqdm.notebook import tqdm

import numpy as np
import pandas as pd


### DB Connection
from model.dbconn import sql_connection
sql_engine = sql_connection()

### Logger
from model.logger import logging, logger, clear_log

logger_module = logging.getLogger('MODEL')
logger_module.setLevel('DEBUG')

### Load
deposits = pd.read_sql('deposit_rates', con=sql_engine)

cbr_rate = pd.read_sql('cbr_rate', con=sql_engine)
cbr_rate.columns = ['date', 'interest_rate']

cbr_inflation = pd.read_sql('cbr_inflation', con=sql_engine)
cbr_inflation.columns = ['date', 'interest_rate', 'inflation', 'inflation_target']

support_data = pd.read_sql('support_data', con=sql_engine)
support_data.columns = ['date', 'rate_mosprime', 'rate_rts', 'rate_brent', 'currency_usd', 'currency_cny']

data = cbr_rate.merge(deposits[['date', 'rate']], how='outer', on='date')
data = data.assign(date = data['date'] + pd.tseries.offsets.MonthEnd(0))
data = data.groupby('date').agg({'rate': 'mean', 'interest_rate': 'mean'})
data['interest_rate'] = data['interest_rate'] * 100
data = data.reset_index()

features = (
    cbr_rate[['date', 'interest_rate']]
        .merge(cbr_inflation[['date', 'inflation']], how='outer', on='date')
        .merge(support_data[['date', 'rate_mosprime', 'rate_rts', 'rate_brent', 'currency_usd', 'currency_cny']], how='outer', on='date')
)
features = features.fillna(method='ffill')
features = features[~features['rate_mosprime'].isnull()]

features['date'] = features['date'] + pd.tseries.offsets.MonthEnd(0)
features = features.groupby('date').mean().reset_index()
features = features.sort_values('date')

date_future = features.iloc[-1:]['date'] + pd.tseries.offsets.MonthEnd(1)
features_future = pd.DataFrame({'date': date_future})
features = pd.concat([features, features_future], ignore_index=True)

features = features.set_index('date')

features_list = list(features.columns)

for feature in features_list:
    for i in range(1,7):
        features[f'{feature}_{i}m'] = features[feature].shift(i)
        
        features[f'{feature}_{i}m_mean'] = 0
        for s in range(1, i+1):
            features[f'{feature}_{i}m_mean'] += features[feature].shift(i)
        features[f'{feature}_{i}m_mean'] = features[f'{feature}_{i}m_mean'] / i

features = features.drop(columns=features_list)
features_list = list(features.columns)

target = deposits.assign(date = deposits['date'] + pd.tseries.offsets.MonthEnd(0))
target = target.groupby('date').agg({'rate': 'mean'})


### Model
from catboost import CatBoostRegressor
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import mutual_info_regression
from sklearn.model_selection import TimeSeriesSplit

# Mutual information-based feature selection
def fs_MutualInformation(X, y, n_features):
    score = mutual_info_regression(X, y)
    score = pd.Series(score)
    score.index = X.columns
    return score.sort_values(ascending=False)[:n_features].index.to_list()

prediction_length = 6
models = {}

for i in range(prediction_length):
    
    logger_module.debug(f'Process model for {i} months')
    
    dataset = features.reset_index()
    dataset['date'] = dataset['date'] + pd.tseries.offsets.MonthEnd(i)
    dataset = dataset.set_index('date')
    dataset = dataset.join(target, how='inner')
    
    n_features = 20
    features_selected = fs_MutualInformation(dataset[features_list], dataset['rate'], n_features)
    
    logger_module.debug(f'Selected features: {features_selected}')

    models[i+1] = CatBoostRegressor(
        iterations=100,
        loss_function='RMSE',
        task_type='CPU',
        logging_level='Silent',
    )

    params = {
      'depth': np.arange(2, 6, 1),
      'min_data_in_leaf' : np.arange(2, 5, 2),
      'l2_leaf_reg' : np.arange(0.0, 0.7, 0.2)
    }

    tscv = TimeSeriesSplit(n_splits=5)
    gs = models[i+1].grid_search(
        params,
        dataset[features_selected],
        dataset['rate'],
        cv=tscv,
        plot=False,
        verbose=False,
    )


logger_params = logging.getLogger('PARAMS')
logger_params.setLevel(log_error_level)
for i in range(1, prediction_length+1):
    logger_module.debug(f'MODEL {i} - {models[i].get_params()}')

result = features.iloc[:-1].join(target, how='inner').reset_index()
result['prediction'] = models[1].predict(result[models[1].feature_names_])

last_date = result.iloc[-1:]['date']

for i in range(1, prediction_length+1):
    date = last_date + pd.tseries.offsets.MonthEnd(i)
    logger_module.debug(f'Predicting for future date - {date}')
    prediction = models[i].predict(features.iloc[-1:][models[i].feature_names_])
    prediction = pd.DataFrame({'date': date, 'rate':None, 'prediction': prediction})
    result = pd.concat([result, prediction], ignore_index=True)

result['date'] = result['date'].astype('datetime64[D]')

result = result[['date', 'rate', 'prediction']]
result.to_sql('result', index=False, con=sql_engine, if_exists='replace')

