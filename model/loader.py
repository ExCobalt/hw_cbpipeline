import time
import datetime
from pathlib import Path
from tqdm import tqdm

import numpy as np
import pandas as pd
import numba as nb
from scipy import stats
import re

import requests
from bs4 import BeautifulSoup
from finam import Exporter, Market, LookupComparator, Timeframe


### DB Connection
from model.dbconn import sql_connection
sql_engine = sql_connection()

### Logger
from model.logger import logging, logger, clear_log

clear_log()
logger_module = logging.getLogger('LOAD')
logger_module.setLevel('DEBUG')

### Load
URL = f"https://www.banki.ru/blog/nogo1/10862.php"

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36'}
page = requests.get(URL, headers=headers)
soup = BeautifulSoup(page.content, "html.parser")

res = soup.find_all("div", {"class": "article-text b-el-article__text"})
links = pd.DataFrame(
    [(i['href'], i.contents[0]) for i in res[0].find_all('a', href=True) 
         if 'Таблица вкладов' in i.contents[0]], columns=['url', 'name']
)

links.to_sql('parsed_links', index=False, con=sql_engine, if_exists='replace')
logger.debug('Saved links to parse')

deposits = pd.DataFrame(columns=['date', 'bank_name', 'deposit_name', 'rate', 'maturity', 'payout', 'other_rates'])

logger.debug('Start loading deposits ratees from www.banki.ru')
for URL in tqdm(links['url']):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36'}
    page = requests.get(URL, headers=headers)
    logger_module.debug(f'Parse {URL} - Response {page}')
    soup = BeautifulSoup(page.content, "html.parser")

    content = [i for i in soup.find_all('meta', {'property': "og:description"}) if 'Таблица ' in i.attrs['content']][0]

    rows = content['content'].split('\r')
    date = re.findall('(\d+.\d+.\d+)', rows[0])[0]

    for row in content['content'].split('\r'):
        if re.findall('\d+?[.|,]\d+?% \(.*%\)', row):
            rates = [float(i.replace(',','.')) for i in re.findall('(\d+?[.|,]\d+)', row) if float(i.replace(',','.')) > 0.1]
            string = re.sub(r'\«[^»]*,[^»]*\»', lambda x: x.group().replace(',', ''), row)
            string = re.sub(r'\([^)]*\)', '', string)
            i_payout = np.inf
            for i, value in enumerate(string.split(',')):
                if 'выплата' in value:
                    try:
                        payout = re.findall('[выплата|выплата/капитализация|капитализация] (\S+? ?\S+)', value)[0]
                        i_payout = i
                    except:
                        payout = None
                        logger_module.debug(f'Not found payout')
                if i == i_payout + 1:
                    try:
                        maturity = re.findall('срок ?\S* (\d+? ?\S+)', value)[0]
                    except:
                        maturity = None
                        logger_module.debug(f'Not found maturity')
                if i == i_payout + 2:
                    try:
                        bank_name = re.findall('^[\w\s]+', value)[0].strip()
                    except:
                        bank_name = None
                        logger_module.debug(f'Not found bank_name')
                if i == i_payout + 3:
                    try:
                        deposit_name = re.findall('«(.*)»', value)[0].strip()
                    except:
                        deposit_name = None
                        logger_module.debug(f'Not found deposit_name')

            deposits = pd.concat([
                deposits, 
                pd.DataFrame.from_dict(
                    {'date': date, 
                     'bank_name': bank_name, 
                     'deposit_name': deposit_name, 
                     'rate': rates[0], 
                     'maturity': maturity, 
                     'payout': payout,
                     'other_rates': tuple(rates[1:]), 
                    }, orient='index').T], 
                ignore_index=True)
            
    logger_module.debug(f'Parsed {date}')
    
    time.sleep(0.5)

deposits['date'] = deposits['date'].replace('31.01.2020', '31.01.2021')
deposits['date'] = deposits['date'].replace('02.10.2020', '30.09.2020')

deposits['date'] = pd.to_datetime(deposits['date'], dayfirst=True)
deposits['rate'] = deposits['rate'].astype(np.float64)
deposits['maturity'] = deposits['maturity'].astype(np.float64)

deposits[['other_rates_1','other_rates_2', 'other_rates_3']] = pd.DataFrame(deposits['other_rates'].tolist(), index=deposits.index).loc[:,:2]
deposits = deposits.drop(columns=['other_rates'])

deposits.to_sql('deposit_rates', index=False, con=sql_engine, if_exists='replace')
logger.debug('Saved deposits rates')


### 1. Процентные ставки и инфляция
def fetch_CBR_rates(date_from, date_to):
    
    URL = f"https://www.cbr.ru/hd_base/KeyRate/?UniDbQuery.Posted=True&UniDbQuery.From={date_from}&UniDbQuery.To={date_to}"
    page = requests.get(URL)
    logger_module.debug(f'Parce {URL}. Response {page}')

    soup = BeautifulSoup(page.content, "html.parser")
    res = soup.find("table", {"class": "data"})
    data = pd.read_html(str(res).replace(",", "."))[0]
    data = data.rename(columns={"Дата": "date", 'Ставка': 'interest_rate'})
    data["date"] = pd.to_datetime(data["date"], format="%d.%m.%Y")
    data = data.set_index("date")
    
    return data / 100


cbr_rate = fetch_CBR_rates("01.01.2016", "01.10.2023")
cbr_rate = cbr_rate.sort_index(ascending=True)
idx = pd.date_range("2016-01-01", "2023-10-01")
cbr_rate = cbr_rate.reindex(idx, method="ffill").reset_index()

cbr_rate.to_sql('cbr_rate', index=False, con=sql_engine, if_exists='replace')
logger.debug('Saved interest rates')


def fetch_CBR_inflation(date_from, date_to):
    
    URL = f"http://www.cbr.ru/Queries/UniDbQuery/DownloadExcel/132934?Posted=True&From={date_from}&To={date_to}&FromDate={date_from}&ToDate={date_to}"
    logger_module.debug(f'Parce {URL}. Response {page}')
    data = pd.read_excel(URL, dtype=object)
    data.columns = ['date', 'interest_rate', 'inflation', 'inflation_target']
    data["date"] = pd.to_datetime(data["date"], format="%m.%Y")
    data = data.set_index("date")
    data = data.astype(np.float64)
    
    return data / 100


cbr_inflation = fetch_CBR_inflation("01.01.2016", "01.10.2023")
cbr_inflation = cbr_inflation.sort_index(ascending=True)
idx = pd.date_range("2016-01-01", "2023-10-01")
cbr_inflation = cbr_inflation.reindex(idx, method="ffill").reset_index()

cbr_inflation.to_sql('cbr_inflation', index=False, con=sql_engine, if_exists='replace')
logger.debug('Saved inflation rates')


### 4. Значения индекса ММВБ, индекса РТС, цены на нефть Brent, курса доллара.

def get_data_finam(codes, market):
    """
    codes: список кодов на ценных бумаг
    market: id рынка на сайте finam, https://github.com/ffeast/finam-export/blob/master/finam/const.py
    """
    exporter = Exporter()
    data_dict = {}

    for code in codes:
        logger_module.debug(f'Process finam {code}')
        lookup = exporter.lookup(code=code, market=market)
        data = exporter.download(
            lookup.index[0],
            market=market,
            start_date=datetime.date(2016, 1, 1),
            end_date=datetime.date(2023, 10, 1),
            timeframe=Timeframe.DAILY,
        )
        data['date'] = data['<DATE>']
        data_dict[lookup.name.iloc[0]] = pd.DataFrame(
            data["<CLOSE>"].values,
            index=pd.to_datetime(data['date'], format="%Y%m%d"),
            columns=[lookup.name.iloc[0]],
        )
    return data_dict


index_code = ["IMOEX", "RTSI"]
oil_code = ["J2TX.FUTBRENTCONT"]
currencies_codes = ["USD000UTSTOM", "CNYRUB_TOM"]  # "EUR_RUB__TOM",

index_data = get_data_finam(index_code, Market.INDEXES)  # Индексы
oil_data = get_data_finam(oil_code, Market.COMMODITIES)  # Нефть
currencies_data = get_data_finam(currencies_codes, Market.CURRENCIES)  # Курсы валют

support_data = pd.concat(
    [
        index_data['Индекс МосБиржи'], 
        index_data['Индекс РТС'], 
        oil_data['Brent Crude Oil Continuous'], 
        currencies_data['USDRUB_TOM'], 
        currencies_data['CNYRUB_TOM']
    ], 
    axis=1
).reset_index()

support_data.to_sql('support_data', index=False, con=sql_engine, if_exists='replace')
logger.debug('Saved finam data')
