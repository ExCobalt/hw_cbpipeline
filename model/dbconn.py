### DB Connection

from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL

CONF_DB = {
    'database'      : 'postgres',
    'user'          : 'postgres',
    'host'          : 'localhost',
    'port'          : '5432',
    'password'      : 'password',
    'container_name': 'postgres',
}

def sql_connection():
    url = URL.create(
        "postgresql+psycopg2",
        username=CONF_DB.get('user'),
        password=CONF_DB.get('password'),
        host=CONF_DB.get('host'),
        port=CONF_DB.get('port'),
        database=CONF_DB.get('database'),
    )
    return create_engine(url)
