import time
import logging
import datetime
from pathlib import Path


### DB Connection
from model.dbconn import sql_connection
sql_engine = sql_connection()


### Logger
def clear_log():
    with sql_engine.connect() as connection:
        connection.execute('DROP TABLE IF EXISTS logs')
        connection.execute(
            '''
            CREATE TABLE logs (
                ID SERIAL PRIMARY KEY,
                log_level int NULL,
                log_levelname char(32) NULL,
                log char(2048) NOT NULL,
                created_at timestamp NOT NULL,
                created_by char(32) NOT NULL
            )
            '''
        )
    return True

Path('./.logs/').mkdir(parents=True, exist_ok=True)

log_error_level     = 'DEBUG'       # LOG error level (file)
log_to_db = True                    # LOG to database?

class LogDBHandler(logging.Handler):
    '''
    Customized logging handler that puts logs to the database.
    pymssql required
    '''
    def __init__(self, sql_conn, db_tbl_log):
        logging.Handler.__init__(self)
        self.sql_conn = sql_conn
        self.db_tbl_log = db_tbl_log

    def emit(self, record):
        # Set current time
        tm = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(record.created))
        # Clear the log message so it can be put to db via sql (escape quotes)
        self.log_msg = record.msg
        self.log_msg = self.log_msg.strip()
        self.log_msg = self.log_msg.replace('\'', '\'\'')
        # Make the SQL insert
        sql = 'INSERT INTO ' + self.db_tbl_log + ' (log_level, ' + \
            'log_levelname, log, created_at, created_by) ' + \
            'VALUES (' + \
            ''   + str(record.levelno) + ', ' + \
            '\'' + str(record.levelname) + '\', ' + \
            '\'' + str(self.log_msg) + '\', ' + \
            '\'' + str(tm) + '\', ' + \
            '\'' + str(record.name) + '\')'
        try:
            with self.sql_conn.connect() as connection:
                connection.execute(sql)
        # If error - print it out on screen. Since DB is not working - there's
        # no point making a log about it to the database :)
        except BaseException as e:
            print(sql)
            print('CRITICAL DB ERROR! Logging to database not possible!')


# Main settings for the database logging use
if (log_to_db):
    # Make the connection to database for the logger
    logdb = LogDBHandler(sql_engine, 'logs')

# Set logger
timestamp = datetime.datetime.now().strftime('%y%m%d%H%M%S')
logging.basicConfig(
    filename=f'./.logs/log_{timestamp}.log',
    filemode='w', level=logging.DEBUG
)

# Set db handler for root logger
if (log_to_db):
    logging.getLogger('').addHandler(logdb)
# Register Logger
logger = logging.getLogger('BASE')
logger.setLevel(log_error_level)

logging.getLogger('urllib3').setLevel(logging.WARN)
logging.getLogger('finam.export').setLevel(logging.WARN)
logging.getLogger('org.openqa.selenium').setLevel(logging.WARN)
